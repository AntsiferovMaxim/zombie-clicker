import {Hero} from "components/hero";
import {HealthBar} from "lib/health-bar";
import {Health} from "../lib/health";
import {getHealthConfig} from "lib/configs/health-bar.config";

export abstract class Unit {
    protected game: Phaser.Game;
    protected body: Phaser.Sprite;
    protected player: Hero;
    protected healthBar: HealthBar;
    protected config: UnitConfig;
    protected health: Health;
    protected isInteractAnimation = false;
    private isInteract = false;

    constructor(game: Phaser.Game, player: Hero, config: UnitConfig) {
        this.game = game;
        this.player = player;
        this.config = config;
        this.health = new Health(config.health);

        this.body = game.add.sprite(this.config.position.x, this.config.position.y, this.config.sprites.walk);
        this.body.animations.add('walk');
        this.body.animations.play('walk', 30, true);
        this.body.anchor.set(.5);
        this.body.name = this.config.index.toString();
        game.physics.enable(this.body, Phaser.Physics.ARCADE);
        this.body.body.immovable = false;
        this.body.body.collideWorldBounds = true;
        this.body.body.bounce.setTo(1, 1);

        if (this.getDirection() > 0) {
            this.body.scale.x = -1;
        }

        this.healthBar = new HealthBar(this.game, getHealthConfig(this.config.health, this.body.position));
        this.game.physics.arcade.moveToXY(this.body, this.game.world.width / 2, this.game.world.height / 2, this.config.speed);
    }

    public update = (): void => {
        const circleCenter = {
            x: this.player.getBody().position.x,
            y: this.player.getBody().position.y + 25
        };

        !this.isInteract &&
        (this.isInteract = distanceBetween(circleCenter, 80, this.body.position));

        this.isInteract && this.interact();

        this.healthBar.setPosition(this.body.x - 48, this.body.y + 30)
    };

    public kill = () => {
        this.body.loadTexture(this.config.sprites.death);
        this.body.animations.add('death');
        this.body.animations.play('death', 30, false, true);
        this.body.body.velocity = 0;

        setTimeout(() => this.healthBar.kill(), 300);
    };

    public damage(size: number) {
        if (size >= this.health.size()) {
            this.health.damage(this.health.size());
            this.healthBar.setCurrentValue(this.health.size());
        }

        this.health.damage(size);
        this.healthBar.setCurrentValue(this.health.size());
    }

    public getHealth() {
        return this.health.size()
    }

    public getBody = () => {
        return this.body;
    };

    public setName = (name: number | string) => {
        this.body.name = name.toString();
    };

    protected getDirection = ():number => {
        return this.game.world.centerX - this.body.position.x;
    };

    abstract interact = (): void => {}
}

function distanceBetween(center: Point, radius: number, point: Phaser.Point) {
    return Math.pow((point.x - center.x), 2) + Math.pow((point.y - center.y), 2) <= Math.pow(radius, 2);
}