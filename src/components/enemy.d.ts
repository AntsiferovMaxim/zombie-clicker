interface EnemyConfig extends UnitConfig {
    damage: Damage
}

interface Damage {
    rate: number,
    size: number,
    time: number
}