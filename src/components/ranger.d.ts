interface RangerConfig extends UnitConfig {
    healSize: number,
    ammunitionCount: number
}