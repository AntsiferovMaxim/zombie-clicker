import {Hero} from "components/hero";
import {Unit} from 'components/unit';

export class Ranger extends Unit {

    constructor(game?: Phaser.Game, player?: Hero, config?: RangerConfig) {
        super(game, player, config);
    }

    public interact = () => {
        if (!this.isInteractAnimation) {
            this.isInteractAnimation = true;
            this.body.loadTexture(this.config.sprites.attack);
            this.body.animations.add('heal');

            this.body.body.velocity !== 0 && (this.body.body.velocity = 0);

            this.player.heal((<RangerConfig>this.config).healSize, (<RangerConfig>this.config).ammunitionCount);
            this.body.animations.play('heal', 30, false, true);
            this.config.killCallback(this);
            setTimeout(() => {
                this.healthBar.kill();
            }, 400);
        }
    };
}