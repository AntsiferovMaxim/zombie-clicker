interface UnitConfig {
    position?: Point,
    sprites: {
        walk: string,
        death: string,
        attack: string
    },
    index?: number,
    health: number,
    speed: number,
    killCallback?: Function
}