import {HealthBar} from "../lib/health-bar";

const sectors = [{
    min: 24,
    max: 68,
    frame: 1,
    inverse: false
}, {
    min: 69,
    max: 113,
    frame: 0,
    inverse: false
}, {
    min: 114,
    max: 158,
    frame: 1,
    inverse: true
}, {
    min: 159,
    max: 203,
    frame: 2,
    inverse: true
}, {
    min: 204,
    max: 248,
    frame: 3,
    inverse: true
}, {
    min: 249,
    max: 293,
    frame: 4,
    inverse: false
}, {
    min: 294,
    max: 338,
    frame: 3,
    inverse: false
}];

export class Hero {
    private game: Phaser.Game;
    private body: Phaser.Sprite;
    private healthBar: HealthBar;
    private healthMax = 10;
    private healthCurrent = this.healthMax;
    private ammunition = 30;

    constructor(index: number, game: Phaser.Game, spriteName: string) {
        this.game = game;
        this.body = game.add.sprite(game.world.centerX, game.world.centerY, spriteName);
        this.body.anchor.set(.5);
        this.body.name = index.toString();

        this.healthBar = new HealthBar(this.game, {
            values: {
                max: this.healthMax,
                min: 0,
                current: this.healthMax
            },
            position: {
                x: this.body.position.x - 50,
                y: this.body.position.y + 90
            },
            colors: {
                background: '#651828',
                bar: '#59ff00'
            },
            size: {
                width: 100,
                height: 10
            }
        });

        game.physics.enable(this.body, Phaser.Physics.ARCADE);
        this.body.body.allowRotation = false;
    }

    public heal = (healSize: number, ammunition: number) => {
        if (healSize + this.healthCurrent >= this.healthCurrent) {
            this.healthCurrent = this.healthMax;
        } else {
            this.healthCurrent += healSize;
        }
        
        this.healthBar.setCurrentValue(this.healthCurrent);
        this.ammunition += ammunition;
    };

    public update = () => {
        const angleInRadians = this.game.physics.arcade.angleToPointer(this.body);
        let angle: number;
        const sprite = {frame: 2, inverse: false};

        if (angleInRadians > 0) {
            angle = Math.floor(Math.abs(angleInRadians * 57.2958));
        } else {
            angle = 180 + 180 - Math.floor(Math.abs(angleInRadians * 57.2958));
        }

        sectors.forEach(item => {
            if (angle < item.max && angle > item.min) {
                sprite.frame = item.frame;
                sprite.inverse = item.inverse;
            }
        });

        this.body.frame = sprite.frame;
        if (sprite.inverse) {
            this.body.scale.x = -1
        } else {
            this.body.scale.x = 1
        }
    };

    public damage = (damageSize: number) => {
        if (damageSize >= this.healthCurrent) {
            this.healthCurrent = 0;
            this.healthBar.setCurrentValue(this.healthCurrent);
            return true;
        }

        this.healthCurrent -= damageSize;
        this.healthBar.setCurrentValue(this.healthCurrent);

        return false;
    };

    public kill = () => {
        this.healthBar.serPercent(0);
    };

    public ammunitionDecrement = () => {
      this.ammunition--;
    };

    public getAmmunition = () => {
        return this.ammunition;
    };

    public getBody = () => {
        return this.body;
    };

    public getHealth = () => {
        return this.healthCurrent;
    };

    public getX = () => {
        return this.body.x;
    };

    public getY = () => {
        return this.body.y;
    };
}