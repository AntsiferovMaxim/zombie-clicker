export class Bullets {
    private group: Phaser.Group;
    private game: Phaser.Game;

    constructor(game: Phaser.Game) {
        this.game = game;

        this.group = game.add.group();
        this.group.enableBody = true;
        this.group.physicsBodyType = Phaser.Physics.ARCADE;

        this.group.createMultiple(50, 'bullet');
        this.group.setAll('checkWorldBounds', true);
        this.group.setAll('outOfBoundsKill', true);
    }

    public addBullet = (x: number, y: number) => {
        const bullet = this.group.getFirstDead();
        bullet.reset(x, y);

        this.game.physics.arcade.moveToPointer(bullet, 300);
    };

    public getGroup = () => {
        return this.group;
    }
}