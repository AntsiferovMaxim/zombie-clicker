import {Hero} from 'components/hero';
import {Health} from 'lib/health';
import {Unit} from "./unit";

export class Enemy extends Unit {
    private enemyDamage: Damage;

    constructor(game: Phaser.Game, player: Hero, config: EnemyConfig) {
        super(game, player, config);
        this.enemyDamage = config.damage;
    }

    public interact = (): void => {
        if (!this.isInteractAnimation) {
            this.isInteractAnimation = true;
            this.body.loadTexture(this.config.sprites.attack);
            this.body.animations.add('attack');
        }

        this.enemyDamage.time === 0 && (this.enemyDamage.time = this.game.time.now);
        this.body.body !== null && this.body.body.velocity !== 0 && (this.body.body.velocity = 0);

        if (this.game.time.now > this.enemyDamage.time) {
            this.enemyDamage.time = this.game.time.now + this.enemyDamage.rate;
            this.body.animations.play('attack', 30, false, false);
            this.player.damage(this.enemyDamage.size);
        }
    };
}