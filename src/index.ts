import {ZombieClicker} from 'game';
import 'assets/favicon.ico';

if (!module.parent) {
    const parent = document.getElementById('zombie-clicker');

    window.onload = () => {
        const config: Phaser.IGameConfig = {
            width: parent.clientWidth,
            height: parent.clientHeight,
            renderer: Phaser.CANVAS,
            parent: 'zombie-clicker'
        };

        new ZombieClicker(config);
    }
}
