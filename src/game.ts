import 'pixi';
import 'p2';
import 'phaser';

import {PreloadState} from 'states/preloader.state';
import {BootState} from 'states/boot.state';
import {MainState} from 'states/main.state';
import {GameOverState} from 'states/game-over.state';

export class ZombieClicker extends Phaser.Game {
    constructor(config: Phaser.IGameConfig) {
        super(config);

        this.state.add('boot', BootState, false);
        this.state.add('preload', PreloadState, false);
        this.state.add('main', MainState, false);
        this.state.add('game-over', GameOverState, false);

        this.state.start('boot');
    }
}