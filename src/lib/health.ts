export class Health {
    private healthMax: number;
    private healthMin: number;
    private healthCurrent: number;

    constructor(health: number) {
        this.healthMax = health;
        this.healthCurrent = health;
        this.healthMin = 0;
    }

    public size (): number {
        return this.healthCurrent;
    }

    public damage = (damageSize: number): number => {
        if (damageSize >= this.healthCurrent) {
            this.healthCurrent = 0;
            return this.healthCurrent;
        }

        this.healthCurrent -= damageSize;

        return this.healthCurrent;
    }
}