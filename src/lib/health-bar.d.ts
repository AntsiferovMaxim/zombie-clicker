interface HealthBarConfig {
    size?: HealthBarSize,
    animationDuration?: number,
    flipped?: boolean,
    isFixedToCamera?: boolean,
    position: HealthBarPosition,
    colors?: HealthBarColors,
    values: HealthBarValues
}

interface HealthBarSize {
    width?: number,
    height?: number,
}

interface HealthBarPosition {
    x: number,
    y: number,
}

interface HealthBarColors {
    background: string,
    bar: string,
}

interface HealthBarValues {
    max: number,
    min: number,
    current: number,
    percent?: number,
}