export function getHealthConfig (health: number, position: Point) {
    return {
        values: {
            max: health,
            min: 0,
            current: health
        },
        position: {
            x: position.x,
            y: position.y + 30
        },
        size: {
            width: 100,
            height: 10
        }
    };
}