export const zombie1Config: EnemyConfig = {
    sprites: {
        walk: 'zombie1-walk',
        death: 'zombie1-death',
        attack: 'zombie1-attack'
    },
    speed: 45,
    health: 1,
    damage: {
        rate: 1000,
        size: 1,
        time: 0
    }
};

export const zombie3Config: EnemyConfig = {
    sprites: {
        walk: 'zombie3-walk',
        death: 'zombie3-death',
        attack: 'zombie3-attack'
    },
    speed: 35,
    health: 3,
    damage: {
        rate: 1000,
        size: 1,
        time: 0
    }
};

export const rangerConfig: RangerConfig = {
    sprites: {
        walk: 'ranger-walk',
        death: 'ranger-death',
        attack: 'ranger-attack'
    },
    speed: 35,
    health: 1,
    healSize: 2,
    ammunitionCount: 5
};