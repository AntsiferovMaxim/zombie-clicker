const defaultConfig: HealthBarConfig = {
    size: {
        width: 250,
        height: 40,
    },
    position: {
        x: 0,
        y: 0,
    },
    colors: {
        background: '#651828',
        bar: '#FEFF03'
    },
    values: {
      max: 5,
      min: 0,
      current: 5,
      percent: 100,
    },
    animationDuration: 200,
    flipped: false,
    isFixedToCamera: false
};

export class HealthBar {
    private config: HealthBarConfig;
    private game: Phaser.Game;
    private backgroundSprite: Phaser.Sprite;
    private barSprite: Phaser.Sprite;

    constructor(game: Phaser.Game, providedConfig?: HealthBarConfig) {
        this.game = game;
        this.config = {...defaultConfig, ...providedConfig};

        this.drawBackground();
        this.drawHealthBar();
    }

    public drawBackground = () => {
        const bmd = this.createBitmapData(this.config.size, this.config.colors.background);

        this.backgroundSprite = this.game.add
            .sprite(
                this.config.position.x,
                this.config.position.y,
                bmd
            );

        if (this.config.flipped) {
            this.backgroundSprite.scale.x = -1;
        }
    };

    public drawHealthBar = () => {
        const bmd = this.createBitmapData(this.config.size, this.config.colors.bar);

        this.barSprite = this.game.add
            .sprite(
                this.config.position.x,
                this.config.position.y,
                bmd
            );

        if (this.config.flipped) {
            this.barSprite.scale.x = -1
        }
    };

    public setPosition = (x: number, y: number) => {
        this.config.position.x = x;
        this.config.position.y = y;

        if (this.backgroundSprite !== undefined && this.barSprite !== undefined) {
            this.backgroundSprite.position.x = x;
            this.backgroundSprite.position.y = y;

            this.barSprite.position.x = x;
            this.barSprite.position.y = y;
        }
    };

    public serPercent = (percent: number) => {
        percent < 0 && (percent = 0);
        percent > 100 && (percent = 100);

        this.config.values.percent = percent;

        this.updateWidth();
    };

    public setCurrentValue = (current: number) => {
        if (current <= this.config.values.min) {
            this.config.values.current = 0;
            this.config.values.percent = 0;
            this.updateWidth();
            return;
        }

        if (current >= this.config.values.max) {
            this.config.values.current = this.config.values.max;
            this.config.values.percent = 100;
            this.updateWidth();
            return;
        }

        this.config.values.percent = current / this.config.values.max * 100;
        this.config.values.current = current;

        this.updateWidth();
    };

    public kill() {
        this.backgroundSprite.kill();
        this.barSprite.kill();
    }

    private updateWidth = () => {
        const newWidth = (this.config.values.percent * this.config.size.width) / 100;

        this.setWidth(newWidth);
    };

    private setWidth = (width: number) => {
        if (this.config.flipped) {
            width = -1 * width;
        }
        
        this.game.add.tween(this.barSprite).to({width}, this.config.animationDuration, Phaser.Easing.Linear.None, true);
    };

    private createBitmapData(size: HealthBarSize, color: string) {
        const bmd = this.game.add.bitmapData(size.width, size.height);

        bmd.ctx.fillStyle = color;
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, size.width, size.height);
        bmd.ctx.fill();
        bmd.update();

        return bmd;
    }
}