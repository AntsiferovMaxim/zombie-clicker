import State from './state';
import * as sprites from 'services/load-images';

export class PreloadState extends State {
    private background: Phaser.TileSprite;

    preload(): void {
        this.game.load.image('bullet', sprites.ballPng);
        this.game.load.image('background', sprites.background);
        this.game.load.image('tower', sprites.tower);
        this.game.load.image('high-score-background', sprites.highScoreBackground);

        this.game.load.image('bullets-icon', sprites.bulletsIcon);
        this.game.load.image('bullets-background', sprites.bulletsBackground);
        this.game.load.image('frag-icon', sprites.fragIcon);
        this.game.load.image('frag-background', sprites.frafBackground);

        this.game.load.spritesheet('cowboy', sprites.cowboySprite, 64, 64, 5);
        this.game.load.spritesheet('zombie1-walk', sprites.zombie1Walk, 64, 64, 64);
        this.game.load.spritesheet('zombie1-death', sprites.zombie1Death, 64, 64, 32);
        this.game.load.spritesheet('zombie1-attack', sprites.zombie1Attack, 64, 64, 32);

        this.game.load.spritesheet('zombie3-walk', sprites.zombie3Walk, 64, 64, 64);
        this.game.load.spritesheet('zombie3-death', sprites.zombie3Death, 64, 64, 32);
        this.game.load.spritesheet('zombie3-attack', sprites.zombie3Attack, 64, 64, 32);

        this.game.load.spritesheet('ranger-walk', sprites.rangerWalk, 64, 64, 32);
        this.game.load.spritesheet('ranger-death', sprites.rangerDeath, 64, 64, 16);
        this.game.load.spritesheet('ranger-attack', sprites.rangerAttack, 64, 64, 32);
    }

    create(): void {
        this.game.state.start('main');
    }
}