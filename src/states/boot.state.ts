import State from './state';

export class BootState extends State {
    create(): void {
        this.game.state.start('preload');
    }
}