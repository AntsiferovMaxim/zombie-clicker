import {ZombieClicker} from '../game';

abstract class State extends Phaser.State {
    game: ZombieClicker
}

export default State;