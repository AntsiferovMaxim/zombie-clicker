import State from 'states/state';
import {Enemy} from 'components/enemy';
import {Hero} from 'components/hero';
import {Bullets} from 'components/bullets';
import {Unit} from 'components/unit';
import * as unitConfigs from 'lib/configs/unit.config';
import {
    getRandomIntByRange,
    getRandomPointOnRectangle
} from 'services/random.service';
import {Ranger} from "components/ranger";

export class MainState extends State {
    private bullets: Bullets;
    private hero: Hero;
    private units: Array<Unit> = [];
    private enemiesMax = 4;
    private deathToll = 0;
    private fireRate = 100;
    private nextFire = 0;
    private background: Phaser.TileSprite;
    private highScoreText: Phaser.Text;
    private ammunitionCountText: Phaser.Text;

    public create(): void {
        this.loadImages();

        this.ammunitionCountText = this.game.add.text(this.world.width - 60, 12, '0', {
            font: '24px Arial',
            fontWeight: 'bold',
            fill: '#ffffff'
        });
        this.ammunitionCountText.stroke = '#000';
        this.ammunitionCountText.strokeThickness = 2;

        this.highScoreText = this.game.add.text(70, 15, '0', {
            font: '25px Arial',
            fontWeight: 'bold',
            fill: '#ffffff',
        });

        this.highScoreText.stroke = '#000';
        this.highScoreText.strokeThickness = 2;

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.hero = new Hero(1, this.game, 'cowboy');

        for (let i = 0; i < this.enemiesMax; i++) {
            this.units.push(this.createUnit(i))
        }

        this.bullets = new Bullets(this.game);

        this.game.input.onTap.add(this.onTap, this);
    }

    public update() {
        this.hero.update();
        this.units.forEach(enemy => enemy.update());

        for (let i = this.units.length - 1; i < this.enemiesMax - 1; i++) {
            this.units.push(this.createUnit(i))
        }

        for (let i = 0; i < this.units.length; i++) {
            this.game.physics.arcade.overlap(this.units[i].getBody(), this.bullets.getGroup(), this.bulletHitRock, null, this);
        }

        if (this.hero.getHealth() === 0) {
            this.hero.kill();
            this.game.state.start('game-over');
        }
    }

    public render() {
        this.highScoreText.text = '' + this.deathToll;
        this.ammunitionCountText.text = '' + this.hero.getAmmunition();
    }

    public fire() {
        if (
            this.game.time.now > this.nextFire
            // && this.ammunition > 0
            && this.bullets.getGroup().countDead() > 0
        ) {
            this.hero.ammunitionDecrement();
            this.nextFire = this.game.time.now + this.fireRate;
            this.bullets.addBullet(this.hero.getX() - 8, this.hero.getY() - 8);
        }
    }

    private unitKill = (unit: Unit) => {
        const lastElementIndex = this.units.length - 1;
        const deletedElementIndex = +unit.getBody().name;

        const last = this.units[lastElementIndex];
        last.setName(deletedElementIndex);

        this.units.splice(deletedElementIndex, 1, last);
        this.units.splice(lastElementIndex, 1);
    };

    private bulletHitRock(sprite: Phaser.Sprite, bullet: Phaser.Sprite) {
        bullet.kill();
        const unit = this.units[+sprite.name];
        unit.damage(1);

        if (unit.getHealth() === 0) {
            this.unitKill(unit);
            this.deathToll++;

            unit.kill();
        }
    }

    private onTap() {
        this.hero.update();
        this.fire();
    }

    private createUnit(index: number): any {
        const config = {
            position: getRandomPointOnRectangle({x: 0, y: 0}, this.world.width, this.world.height),
            index,
            killCallback: this.unitKill
        };
        
        if (getRandomIntByRange(0, 10) > 2) {
            if (getRandomIntByRange(0, 6) > 2) {
                return new Enemy(this.game, this.hero, {...unitConfigs.zombie3Config, ...config})
            } else {
                return new Enemy(this.game, this.hero, {...unitConfigs.zombie1Config, ...config})
            }
        } else {
            return new Ranger(this.game, this.hero, {...unitConfigs.rangerConfig, ...config})
        }
    }

    private loadImages() {
        this.game.add.image(0, 0, 'high-score-background');
        this.background = this.game.add.tileSprite(0, 0, this.world.width, this.world.height, 'background');
        this.game.add.image(this.world.centerX - 70, this.world.centerY - 27, 'tower')
            .scale.set(.7);

        const backgroundHeight = 1080;
        const backgroundWidth = 1920;
        const heightCoefficient = this.game.world.height / backgroundHeight;
        const widthCoefficient = this.game.world.width / backgroundWidth;
        this.background.tileScale.x = widthCoefficient;
        this.background.tileScale.y = heightCoefficient;

        const fragBackground = this.game.add.image(10, 10, 'frag-background');
        fragBackground.scale.set(.3);
        fragBackground.alpha = .5;

        this.game.add.image(25, 17, 'frag-icon').scale.set(.38);

        const bulletsBackground = this.game.add.image(this.game.world.width - 110, 10, 'bullets-background');
        bulletsBackground.scale.set(.35);
        bulletsBackground.alpha = .5;

        this.game.add.image(this.game.world.width - 100, 15, 'bullets-icon').scale.set(.32);
    }
}