import State from './state';

export class GameOverState extends State {
    private gameOverText: Phaser.Text;

    create() {
        this.gameOverText = this.game.add.text(this.game.world.centerX - 150, this.game.world.centerY - 100, ' ', {font: '42px Arial', fill: '#F2F2F2'});
    }

    update() {
        this.gameOverText.text = 'Game over\nClick to restart';
        this.gameOverText.visible = true;
        
        this.game.input.onTap.addOnce(() => {
            this.game.state.start('main', false, true);
        }, this);
    }
}