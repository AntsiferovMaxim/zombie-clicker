const ballPng = require('assets/sprites/purple_ball.png');

const cowboySprite = require('assets/characters/cowboy/guy_atlas.png');

const zombie1Walk = require('assets/characters/zombie1/zombie_small_move_atlas.png');
const zombie1Death = require('assets/characters/zombie1/zombie_small_death_atlas.png');
const zombie1Attack = require('assets/characters/zombie1/small_zombie_attack_atlas.png');

const zombie3Walk = require('assets/characters/zombie3/zombie_fat_move_atlas.png');
const zombie3Death = require('assets/characters/zombie3/zombie_fat_fall_death_atlas.png');
const zombie3Attack = require('assets/characters/zombie3/zombie_fat_attack_atlas.png');

const rangerWalk = require('assets/characters/ranger/unzombie_move_atlas.png');
const rangerDeath = require('assets/characters/ranger/unzombie_death_atlas.png');
const rangerAttack = require('assets/characters/ranger/unzombie_jump_atlas.png');

const background = require('assets/environment/Zombies_Background.png');
const highScoreBackground = require('assets/ui/high_score_back.png');
const tower = require('assets/core/tower.png');

const bulletsIcon = require('assets/core/ui/core_bullets.png');
const bulletsBackground = require('assets/core/ui/core_bullets_holder.png');
const fragIcon = require('assets/core/ui/frag_skull.png');
const frafBackground = require('assets/core/ui/frags_background.png');

export {
    bulletsIcon,
    bulletsBackground,
    fragIcon,
    frafBackground,
    zombie1Attack,
    zombie1Death,
    zombie1Walk,
    zombie3Attack,
    zombie3Death,
    zombie3Walk,
    rangerWalk,
    rangerDeath,
    rangerAttack,
    highScoreBackground,
    tower,
    cowboySprite,
    background,
    ballPng,
}