export function getRandomIntByRange(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) + min);
}

export function getRandomPointOnRectangle(start: Point, width: number, height: number) {
    const isTop = getRandomIntByRange(0, 2) > 0;
    const isLeft = getRandomIntByRange(0, 2) > 0;
    const isVertical = getRandomIntByRange(0, 2) > 0;
    let point;

    if (isVertical) {
        if (isTop) {
            point = {
                x: getRandomIntByRange(0 + start.x, width + start.x),
                y: 0 + start.y
            }
        } else {
            point = {
                x: getRandomIntByRange(0 + start.x, width + start.x),
                y: height + start.y
            }
        }
    } else {
        if (isLeft) {
            point = {
                x: 0 + start.x,
                y: getRandomIntByRange(0 + start.y, height + start.y)
            }
        } else {
            point = {
                x: width + start.x,
                y: getRandomIntByRange(0 + start.y, height + start.y)
            }
        }
    }

    return point;
}

export function getRandomPointOnCircle(radius: number, center: Point): Point {
    const randomAngle = Math.floor(Math.random() * 360);

    return getEndVector(center, radius, randomAngle);
}

function getEndVector(start: Point, length: number, angle: number) {
    const radian = angle * Math.PI / 180;

    const x = start.x + Math.cos(radian) * length;
    const y = start.y + Math.sin(radian) * length;

    return {x, y}
}