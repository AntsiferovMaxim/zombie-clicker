const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const src = path.join(__dirname, 'src');
const entry = path.join(src, 'index.ts');
const dist = path.join(__dirname, 'dist');

const phaserHome = path.join(__dirname, '/node_modules/phaser-ce');
const phaser = path.join(phaserHome, '/build/custom/phaser-split.js');
const pixi = path.join(phaserHome, '/build/custom/pixi.js');
const p2 = path.join(phaserHome, '/build/custom/p2.js');

module.exports = {
    entry: {
        main: [entry]
    },
    output: {
        path: dist,
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            use: ['babel'],
            include: path.join(__dirname, 'src')
        }, {
            test: /\.ts$/,
            use: 'ts',
            exclude: '/node_modules/'
        }, {
            test: /pixi\.js/,
            use: [{
                loader: 'expose',
                options: 'PIXI'
            }]
        }, {
            test: /phaser-split\.js/,
            use: [{
                loader: 'expose',
                options: 'Phaser'
            }]
        }, {
            test: /p2\.js/,
            use: [{
                loader: 'expose',
                options: 'p2'
            }]
        }, {
            test: /\.(png|svg|jpg|gif)$/,
            use: [{
                loader: 'file',
                options: {
                    name: 'assets/img/[name].[ext]',
                }
            }]
        }, {
            test: /\.ico$/,
            use: [{
                loader: 'file',
                options: {
                    name: '[name].[ext]'
                }
            }]
        }]
    },
    resolve: {
        extensions: ['.js', '.json', '.ts'],
        alias: {
            phaser: phaser,
            pixi: pixi,
            p2: p2
        },
        modules: [
            'node_modules',
            src
        ]
    },
    resolveLoader: {
        modules: ['node_modules'],
        moduleExtensions: ['-loader']
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: path.join(src, 'index.html'),
            chunks: ['vendor', 'main'],
            chunksSortMode: 'manual',
            minify: {
                removeAttributeQuotes: false,
                collapseWhitespace: false,
                html5: false,
                minifyCSS: false,
                minifyJS: false,
                minifyURLs: false,
                removeComments: false,
                removeEmptyAttributes: false
            },
            hash: false
        })
    ]
};

if (process.env.NODE_ENV === 'development') {
    module.exports.plugins.push(new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"development"'
        }
    }));
    module.exports.watch = true;
    module.exports.devtool = 'eval';
    module.exports.watchOptions = {
        ignored: '/node_modules/',
        aggregateTimeout: 100
    };
    module.exports.devServer = {
        contentBase: dist,
        open: true,
        port: 3242
    };
}

if (process.env.NODE_ENV === 'production') {
    module.exports.plugins.push(new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: '"production"'
        }
    }));
    module.exports.plugins.push(new CleanWebpackPlugin([dist]));
}